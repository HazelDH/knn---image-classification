"""

------Module description------

KNeighborsClassifier - implementation of KNN classifier
LabelEncoder - to convert strings(labels) to int, unique int for class
train_test_split - splitting data into test Nd training groups
classification_report - evaluating performance, 

------------------------------

"""

from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder 
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from Preprocessor import Preprocessor
from DataLoader import DataLoader
from imutils import paths
import argparse



"""
Argument parse, loading hyperparameters

"""

vArgPar = argparse.ArgumentParser()
vArgPar.add_argument("-d","--dataset",required=True)
vArgPar.add_argument("-k","--neighbors",type=int,default=1)
vArgPar.add_argument("-j","--jobs",type=int,default=-1)
args = vars(vArgPar.parse_args())


def main():
    print("Starting programm...")
    vImagePaths = list(paths.list_images(args["dataset"]))

    vPreprocessing = Preprocessor(32,32)
    print("Loading images...")
    vDataLoader = DataLoader(vPreprocessing)

    (vData,vLabels) = vDataLoader.load(vImagePaths,verbose=500)

    vData = vData.reshape((vData.shape[0],3072))

    print("[INFO] features matrix: {:.1f}MB".format(vData.nbytes / (1024 * 1000.0)))

    vLabEncoder = LabelEncoder()
    vLabels = vLabEncoder.fit_transform(vLabels)

    #Partition of the data, test data = 25%
    (trainX,testX,trainY,testY) = train_test_split(vData,vLabels,test_size=0.3,random_state=42)

    print("[INFO] evaluating k-NN classifier...")
    vModel = KNeighborsClassifier(n_neighbors=args["neighbors"],n_jobs=args["jobs"])
    vModel.fit(trainX, trainY)
    print(classification_report(testY, vModel.predict(testX),target_names=vLabEncoder.classes_))


#Starting main function
if __name__ == '__main__':
    main()
    

