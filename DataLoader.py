import numpy as np
import cv2 as cv
import os


"""
------- WARNING!!! -------

This algorithm assumes, that all data(images and thier labels)
fits into operated memoery, there is no chunk of data in this procesing
Keep that in mind!

--------------------------
"""
class DataLoader:
    def __init__(self, Preprocessors=None):
        self.preprocessors = Preprocessors
        if self.preprocessors is None:
            #if preprocessor is empty, initialize it as empty list
            self.preprocessors = []
    def load(self,ImagePath,verbose=-1):
        mData = []
        mLabels = []

        #WARNING
        #this loop loops over the images and extracts classlabel 
        for (i,imPath) in enumerate(ImagePath):
            tImage = cv.imread(imPath)
            tLabel = imPath.split(os.path.sep)[-2]
            #Looping over the preprocessors and apply them into the image:
            if self.preprocessors is not None:
                #for p in self.preprocessors:
                tImage = self.preprocessors.preprocess(tImage)
            #Adding preprocessed images and their labels
            mData.append(tImage)
            mLabels.append(tLabel)

            if verbose > 0 and i > 0 and (i + 1) % verbose == 0:
                print("processed: {}/{}".format(i+1,len(ImagePath)))
        #returnn preprocessed images and thier labels in form of numpy arrays
        return (np.array(mData), np.array(mLabels))
